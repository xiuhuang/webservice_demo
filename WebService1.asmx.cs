﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebApplication1
{
    /// <summary>
    /// WebService1 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod(Description = "获取当前系统时间")]
        public string GetTime(string input)
        {
            return DateTime.Now.ToString();
        }

        [WebMethod(Description = "实体类测试")]
        public Result GetValue(int ID, string Name)
        {
            Result result = new Result();
            List<Test> testlist = new List<Test>();
            testlist.Add(new Test(1, "一"));
            testlist.Add(new Test(2, "二"));
            testlist.Add(new Test(ID, Name));
            result.data = testlist;
            return result;
        }

        [WebMethod(Description = "实体类测试New")]
        public List<Test> GetValueNew(int ID, string Name)
        {
            List<Test> testlist = new List<Test>();
            testlist.Add(new Test(1, "一"));
            testlist.Add(new Test(2, "二"));
            testlist.Add(new Test(ID, Name));
            return testlist;
        }
    }
}
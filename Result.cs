﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{

    public class Result
    {
        public List<Test> data { get; set; }

    }
    public class Test
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public Test()
        {

        }

        public Test(int ID, string Name)
        {
            this.ID = ID;
            this.Name = Name;
        }
    }
}